package com.rods.concrete.concretedesafio.viper;

public class BaseContracts {

    public interface Presenter {
        void detach();
        void detachUI();
        void attachUI();
    }

}
