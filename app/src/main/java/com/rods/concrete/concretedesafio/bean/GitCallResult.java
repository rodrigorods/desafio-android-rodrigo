package com.rods.concrete.concretedesafio.bean;

import java.util.List;

public class GitCallResult {
    private List<Repository> items;

    public List<Repository> getItems() {
        return items;
    }
}
