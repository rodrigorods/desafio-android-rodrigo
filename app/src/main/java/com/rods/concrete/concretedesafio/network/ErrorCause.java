package com.rods.concrete.concretedesafio.network;

public enum ErrorCause {
    NO_INTERNET_ERROR,
    UNKNOWN_ERROR
}
